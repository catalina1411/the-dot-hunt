﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour {
    [SerializeField] ParticleSystem effect;
    [SerializeField] bool intanciar;
    [SerializeField] GameObject[] ithems;
    int rango;
    [Range(0,100)]
    [SerializeField] int posibilidad;
    [Header("Vida")] [SerializeField] int resistencia;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(resistencia<= 0)
        {
            Destruir();
        }
	}
    public void SartDestroy(int ammount)
    {
        resistencia -= ammount;
    }
    void Destruir()
    {
        print("Destruido");
        Instantiate(effect, transform.position, Quaternion.EulerAngles(-90, 0, 0));
        rango = Random.Range(0, 100);
        if (rango <= posibilidad)
        {
            int finl = Random.Range(0, ithems.Length);
            Instantiate((ithems[finl]), transform.position + transform.up, transform.rotation);
        }
        Destroy(gameObject);

    }
}
