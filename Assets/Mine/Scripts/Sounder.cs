﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounder : MonoBehaviour {
    AudioSource source;
    [SerializeField] AudioClip[] WeaponClips;
    [SerializeField] AudioClip damage, heal, death;
	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
    public void PlayThis(int spud)
    {
        source.PlayOneShot(WeaponClips[spud]);
    }
    public void PlayDamage()
    {
        source.PlayOneShot(damage);
    }
    public void PlayThisClip(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }
    public void PlayHeal()
    {
        source.PlayOneShot(heal);
    }
    public void PlayDeath()
    {
        source.PlayOneShot(death);
    }
}
