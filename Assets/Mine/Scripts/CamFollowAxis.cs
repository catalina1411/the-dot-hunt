﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollowAxis : MonoBehaviour
{
    enum Type { _FullTopDown, _FrontTopDown, _SideTopDown }
    [SerializeField] Type _Type;
    NewMineTird player1;
    [SerializeField] Vector2 offset;
    [SerializeField] float speed;
    void Start()
    {
        player1 = FindObjectOfType<NewMineTird>();
    }
    void Update()
    {
        if (_Type == Type._FullTopDown)
        {
            print("FullTopDown");
            if (transform.position.x < player1.transform.position.x - offset.x
                || transform.position.x > player1.transform.position.x + offset.x
                || transform.position.z < player1.transform.position.z - offset.y
                || transform.position.z > player1.transform.position.z + offset.y)
            {
                print("Trying Follow");
                Vector3 tarjectPos = new Vector3(player1.transform.position.x, transform.position.y, player1.transform.position.z);
                Vector3 newpos = Vector3.Lerp(transform.position, tarjectPos, speed * Time.deltaTime);
                transform.position = newpos;
            }

        }
    }
}