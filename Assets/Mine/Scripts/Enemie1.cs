﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using AkamaLife;
public class Enemie1 : MonoBehaviour {
    enum HAbilitie { axe, sword, bow, magic}
    [SerializeField] HAbilitie abilitie;
    Animator anim;
    [SerializeField] Damager d_Sword, d_Axe;
    [SerializeField] Bow b_Bow;
    [SerializeField]
    float timeToAttackAgain;
    float speed = 2f, runSpeed = 5f, groundDistance = 0.5f;
    MineLifer lifer;
    Rigidbody rb;
    NavMeshAgent agent;
    bool running, attaking, grounded, jump, alive;
    Transform tarect;
    bool axe, bow, sword, magic;
    int currentAbilitie;

    void Start () {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();
        lifer = GetComponent<MineLifer>();
        agent.enabled = false;
        Habilitie();
        alive = true;
    }

    // Update is called once per frame
    void Update () {
        if (alive)
        {
            if (lifer.currentLife <= 0f)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetTrigger("Death");
                CapsuleCollider[] collid = GetComponentsInChildren<CapsuleCollider>();
                for (int a = 1; a < collid.Length; a++)
                {
                    collid[a].enabled = false;
                }
                StartCoroutine(WaitToDestroy());
                alive = false;

            }

            anim.SetBool("Ground", grounded);
            Grounder();
            CheckForAttack();
            if(agent.enabled == true)
            {
                agent.SetDestination(tarect.position);
                anim.SetFloat("FordWard", 1);
            }
            if ((lifer.currentLife > 0) && (!attaking) && (sword || axe || bow))
                anim.SetLayerWeight(1, 1);
        }
    }
    public void AnimateThis(string trigger)
    {
        anim.SetTrigger(trigger);
    }
    public void Agent(Transform tarject)
    {
        agent.enabled = true;
        tarect = tarject;
    }
    public void  DisAgent()
    {
        tarect = null;
        agent.enabled = false;
    }

    private void Grounder()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, out hitInfo, groundDistance))
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.blue);
            grounded = true;

        }
        else
        {
            Debug.DrawLine(transform.position + (Vector3.up * 0.1f), transform.position + (Vector3.up * 0.1f) + (Vector3.down * groundDistance), Color.red);
            grounded = false;
            attaking = false;
        }
    }
    void Habilitie()
    {
        switch (abilitie)
        {
            case HAbilitie.axe:
                axe = true;
                bow = false;
                sword = false;
                magic = false;
                break;
            case HAbilitie.bow:
                axe = false;
                bow = true;
                sword = false;
                magic = false;
                break;
            case HAbilitie.magic:
                axe = false;
                bow = false;
                sword = false;
                magic = true;
                break;
            case HAbilitie.sword:
                axe = false;
                bow = false;
                sword = true;
                magic = false;
                break;
        }
        anim.SetBool("Axe", axe);
        anim.SetBool("Bow", bow);
        anim.SetBool("Sword", sword);
        anim.SetBool("Magic", magic);
    }
    void CheckForAttack()
    {
        if(agent.enabled == true)
        {
            if (Vector3.Distance(transform.position,tarect.position) < (agent.stoppingDistance * 1.1f) && !attaking)
            {
                anim.SetLayerWeight(1, 0);
                anim.SetTrigger("Fire1");
                attaking = true;
            }
        }
    }
    public void AbleToAttack()
    {
        if (sword)
            d_Sword.GetComponent<BoxCollider>().enabled = false;
        if (axe)
            d_Axe.GetComponent<BoxCollider>().enabled = false;
        StartCoroutine(WaitToAttackAgain());
    }
    void ActiveWeapon()
    {
        if (sword)
            d_Sword.GetComponent<BoxCollider>().enabled = true;
        if (axe)
            d_Axe.GetComponent<BoxCollider>().enabled = true;
        if (bow)
            b_Bow.FireBow();

    }
    IEnumerator WaitToAttackAgain()
    {
        yield return new WaitForSeconds(timeToAttackAgain);
        attaking = false;
    }
    IEnumerator WaitToDestroy()
    {
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

}
