﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
public class Damager : MonoBehaviour {
    [SerializeField] int potencia = 1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (GetComponentInParent<NewMineTird>() == null)
        {
            MineLifer lifer = other.GetComponent<MineLifer>();
            MineLifer liferp = other.gameObject.GetComponentInParent<MineLifer>();
            if (lifer != null && other.GetComponentInParent<NewMineTird>() != null)
            {
                lifer.Damage(potencia);
                return;
            }
            if (liferp != null && other.GetComponentInParent<NewMineTird>() != null)
            {
                liferp.Damage(potencia);
                return;
            }
        }
        else
        {
            MineLifer lifer = other.GetComponent<MineLifer>();
            MineLifer liferp = other.gameObject.GetComponentInParent<MineLifer>();

            if (lifer != null && other.GetComponentInParent<Enemie1>() != null)
            {
                lifer.Damage(potencia);
                return;
            }
            if (liferp != null && other.GetComponentInParent<Enemie1>() != null)
            {
                liferp.Damage(potencia);
                return;
            }

            Destroyable terject = other.GetComponent<Destroyable>();
            if (terject != null)
            {
                terject.SartDestroy(potencia);
                return;
            }
        }
    }
}
