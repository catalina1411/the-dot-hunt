﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AkamaLife;
public class Item : MonoBehaviour {
    public enum ItemClass{coin, healthPotion, magicPotion, axe, bow, sword, shield}
    public ItemClass tipo;
    [SerializeField] AudioClip clip;
    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IthemsCounter>()!= null)
        {
            switch(tipo)
            {
                case ItemClass.coin:
                    other.GetComponent<IthemsCounter>().coins+=1;
                    break;
                case ItemClass.healthPotion:
                    other.GetComponent<MineLifer>().Heal(1);
                    break;
                case ItemClass.magicPotion:
                    other.GetComponent<IthemsCounter>().magicPotions +=1;
                    break;
                case ItemClass.axe:
                    other.GetComponent<IthemsCounter>().axe = true;
                    other.GetComponent<IthemsCounter>().just = true;
                    break;
                case ItemClass.bow:
                    other.GetComponent<IthemsCounter>().bow = true;
                    other.GetComponent<IthemsCounter>().just = true;
                    break;
                case ItemClass.shield:
                    other.GetComponent<IthemsCounter>().shield = true;
                    other.GetComponent<IthemsCounter>().just = true;
                    break;
                case ItemClass.sword:
                    other.GetComponent<IthemsCounter>().sword = true;
                    other.GetComponent<IthemsCounter>().just = true;
                    break;
            }
            other.GetComponent<Sounder>().PlayThisClip(clip);
            Destroy(gameObject);
        }
    }
}
