﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using AkamaLife;
using UnityEngine.UI;
public class Manager : MonoBehaviour {
    [SerializeField] GameObject mesage;
    [SerializeField] NewMineTird player;

    MineLifer playerLife;
    IthemsCounter playerCounter;
    Text messageText;
    MineImputer imp;
    bool start, win;
    [SerializeField] Item[] coins;

    // Use this for initialization
    void Start () {
        playerLife = player.GetComponent<MineLifer>();
        playerCounter = player.GetComponent<IthemsCounter>();
        imp = player.GetComponent<MineImputer>();
        messageText = mesage.GetComponentInChildren<Text>();
        messageText.text = "Hola, Soy Edward, Ana Catalina gonzalez me contrato para recoger todas las calabazas, pero hay ladrones por todas partes! Ayudame a encontrarlas!";
        mesage.SetActive(true);
        start = true;
        StartCoroutine(WaitToKillMEsage(5));
    }
	
	// Update is called once per frame
	void Update () {
        if (playerCounter.just)
        {
            messageText.text = "Nueva arma!";
            mesage.SetActive(true);
            StartCoroutine(WaitToKillMEsage(1));
        }
        if(!playerLife.alive && !start)
        {
            StartCoroutine(WaitToReload());
            
        }
        Winner();
        if (win)
        {
            messageText.text = "Lo hemos logrado!, Gracias por tu ayuda!";
            mesage.SetActive(true);
            StartCoroutine(WaitToReload());
        }
    }
    void Winner()
    {
        foreach (Item coin in coins)
        {
            if (coin != null)
            {
                win = false;
                return;
            }
        }
        win = true;
    }
    IEnumerator WaitToKillMEsage(float time)
    {
        start = false;
        yield return new WaitForSeconds(time);
        mesage.SetActive(false);
    }
    IEnumerator WaitToReload()
    {
        print("Contando");
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(0);
    }
}
